module Soma::Formatter
  abstract def call(
    severity : Severity,
    datetime : Time,
    progname : String,
    message : String,
    exception : Exception?,
    extra : Hash,
    io : IO
  )
end
