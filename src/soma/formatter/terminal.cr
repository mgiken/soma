require "colorize"

class Soma::Formatter::Terminal
  include Formatter

  private COLOR_BY_SEVERITY = {
    Severity::DEBUG   => :dark_gray,
    Severity::INFO    => :light_green,
    Severity::WARN    => :light_yellow,
    Severity::ERROR   => :light_red,
    Severity::FATAL   => :light_magenta,
    Severity::UNKNOWN => :white,
  }

  def call(severity, datetime, progname, message, exception, extra, io)
    color = COLOR_BY_SEVERITY[severity]

    io << datetime.to_s("%Y-%m-%d | %H:%M:%S.%3N")
    io << " | " << Thread.current.object_id.to_s(36)
    io << " | " << (severity.unknown? ? "ANY" : severity.to_s).ljust(5).colorize(color)
    io << " | " << message.ljust(40)

    extra.each do |k, v|
      io << " " << k.colorize(color) << "=" << v
    end

    if exception
      io.puts
      io << exception.inspect_with_backtrace.chomp.split("\n").map(&.colorize(color)).join("\n")
    end
  end
end
