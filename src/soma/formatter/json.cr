require "json"

class Soma::Formatter::JSON
  include Formatter

  def call(severity, datetime, progname, message, exception, extra, io)
    payload = extra.merge({
      :time     => datetime,
      :level    => severity.unknown? ? "any" : severity.to_s.downcase,
      :hostname => System.hostname,
      :progname => progname,
      :pid      => Process.pid,
      :thread   => Thread.current.object_id.to_s(36),
      :msg      => message,
    })

    if exception
      payload[:err] = exception.class.name
      payload[:bt] = exception.inspect_with_backtrace.chomp
    end

    io << payload.to_json
  end
end
