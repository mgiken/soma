require "../ext/logger"
require "./formatter/*"

class Soma::Logger < ::Logger
  @@mutex_by_io = {} of IO | Nil => Mutex

  protected setter fields = {} of String => JSON::Any

  def initialize(
    @io : IO?,
    @level = Severity::INFO,
    @formatter = @io.try(&.tty?) ? Soma::Formatter::Terminal.new : Soma::Formatter::JSON.new,
    @progname = File.basename(PROGRAM_NAME),
    **fields
  )
    @closed = false
    @mutex = @@mutex_by_io[@io] ||= Mutex.new
    @fields = JSON.parse(fields.to_json).as_h unless fields.empty?
  end

  def child(**fields)
    self.class.new(@io, @level, @formatter, @progname).tap do |it|
      it.fields = @fields.merge(JSON.parse(fields.to_json).as_h)
    end
  end

  def force_human_readable!
    @formatter = Soma::Formatter::Terminal.new unless @formatter.is_a?(Soma::Formatter::Terminal)
  end

  def force_json!
    @formatter = Soma::Formatter::JSON.new unless @formatter.is_a?(Soma::Formatter::JSON)
  end

  {% for name in Severity.constants %}
    def {{name.id.downcase}}(message, progname = nil, **extra)
      log(Severity::{{name.id}}, message, progname, **extra)
    end

    def {{name.id.downcase}}(message, exception : Exception, progname = nil, **extra)
      log(Severity::{{name.id}}, message, exception, progname, **extra)
    end
  {% end %}

  def log(severity, message, progname = nil, **extra)
    return if severity < level || !@io
    write(severity, Time.local, progname || @progname, message, **extra)
  end

  def log(severity, message, exception : Exception, progname = nil, **extra)
    return if severity < level || !@io
    write(severity, Time.local, progname || @progname, message, exception, **extra)
  end

  def log(severity, exception : Exception, progname = nil, **extra)
    return if severity < level || !@io
    write(severity, Time.local, progname || @progname, exception.message, exception, **extra)
  end

  private def write(severity, datetime, progname, extra : Tuple(T, NamedTuple)) forall T
    write(severity, datetime, progname, extra.first, **extra.last)
  end

  private def write(severity, datetime, progname, message, exception = nil, **extra)
    io = @io
    return unless io

    progname_to_s = progname.to_s
    message_to_s = message.to_s
    fields = @fields.merge(extra.to_h)
    @mutex.synchronize do
      formatter.call(severity, datetime, progname_to_s, message_to_s, exception, fields, io)
      io.puts
      io.flush
    end
  end
end
