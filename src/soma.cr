require "./soma/logger"

module Soma
  VERSION = {{ `shards version #{__DIR__}`.stringify.chomp }}

  alias Severity = Logger::Severity

  def self.logger
    @@logger ||= Logger.new(STDOUT)
  end
end
