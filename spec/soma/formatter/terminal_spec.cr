require "../../spec_helper"

describe Soma::Formatter::Terminal do
  describe "#call" do
    it "output with a human readable format" do
      io = IO::Memory.new

      Soma::Formatter::Terminal.new.call(
        Logger::INFO, Time.local, "progname", "message", nil, {:foo => "foo", :bar => 1}, io
      )

      actual = io.to_s
      expect = /.+ \| .+ \| .+ \| \e\[92mINFO \e\[0m \| message                                  \e\[92mfoo\e\[0m=foo \e\[92mbar\e\[0m=1/

      actual.should match expect
    end
  end
end
