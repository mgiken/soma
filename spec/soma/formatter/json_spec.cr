require "../../spec_helper"

describe Soma::Formatter::JSON do
  describe "#call" do
    it "output with a json format" do
      io = IO::Memory.new

      Soma::Formatter::JSON.new.call(
        Logger::INFO, Time.local, "progname", "message", nil, {:foo => "foo", :bar => 1}, io
      )

      actual = io.to_s
      expect = /{"foo":"foo","bar":1,"time":".+","level":"info","hostname":".+","progname":"progname","pid":\d+,"thread":".+","msg":"message"}/ # ameba:disable Layout/LineLength

      actual.should match expect
    end
  end
end
