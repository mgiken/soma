require "../spec_helper"

describe Soma::Logger do
  describe ".new" do
    context "with nil" do
      it "can create a logger" do
        logger = Soma::Logger.new(nil)
        logger.error("message")
      end

      it "doesn't yield to the block" do
        a = 0
        logger = Soma::Logger.new(nil)
        logger.info { a = 1 }
        a.should eq(0)
      end
    end
  end

  describe "#child" do
    context "with fields" do
      it "can create a child logger" do
        IO.pipe do |r, w|
          parent = Soma::Logger.new(w, child: false)
          parent.info("parent")

          child = parent.child(child: true)
          child.info("child")

          w.close

          r.gets.should match(/"child":false,.*"msg":"parent"/)
          r.gets.should match(/"child":true,.*"msg":"child"/)
          r.gets.should be_nil
        end
      end
    end

    context "without fields" do
      it "can create a child logger" do
        IO.pipe do |r, w|
          parent = Soma::Logger.new(w)
          parent.info("parent")

          child = parent.child
          child.info("child")

          w.close

          r.gets.should match(/"msg":"parent"/)
          r.gets.should match(/"msg":"child"/)
          r.gets.should be_nil
        end
      end
    end
  end

  describe "#force_human_readable!" do
    it "change to terminal formater" do
      logger = Soma::Logger.new(nil, formatter: Soma::Formatter::JSON.new)
      logger.formatter.should be_a(Soma::Formatter::JSON)

      logger.force_human_readable!
      logger.formatter.should be_a(Soma::Formatter::Terminal)
    end
  end

  describe "#force_json!" do
    it "change to json formater" do
      logger = Soma::Logger.new(nil, formatter: Soma::Formatter::Terminal.new)
      logger.formatter.should be_a(Soma::Formatter::Terminal)

      logger.force_json!
      logger.formatter.should be_a(Soma::Formatter::JSON)
    end
  end

  describe "formats message" do
    context "with number" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info(12_345)
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[time level hostname progname pid thread msg]
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("12345")

          r.gets.should be_nil
        end
      end
    end

    context "only message" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info("message")
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[time level hostname progname pid thread msg]
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("message")

          r.gets.should be_nil
        end
      end
    end

    context "with progname" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info("message", "PROGNAME")
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[time level hostname progname pid thread msg]
          actual["level"].should eq("info")
          actual["progname"].should eq("PROGNAME")
          actual["msg"].should eq("message")

          r.gets.should be_nil
        end
      end
    end

    context "with extra" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info("message", foo: "foo", bar: 1)
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[foo bar time level hostname progname pid thread msg]
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("message")
          actual["foo"].should eq("foo")
          actual["bar"].should eq(1)

          r.gets.should be_nil
        end
      end
    end

    context "with exception" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          begin
            raise "error"
          rescue e : Exception
            logger.info("message", e)
          end
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[time level hostname progname pid thread msg err bt]
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("message")
          actual["err"].should eq("Exception")
          actual["bt"].as_s.should start_with("error (Exception")

          r.gets.should be_nil
        end
      end
    end

    context "only exception" do
      it "logs formatted" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          begin
            raise "error"
          rescue e : Exception
            logger.info(e)
          end
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w[time level hostname progname pid thread msg err bt]
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("error")
          actual["err"].should eq("Exception")
          actual["bt"].as_s.should start_with("error (Exception")

          r.gets.should be_nil
        end
      end
    end
  end

  describe "yields message" do
    context "only message" do
      it "yields" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info { "mess" + "age" }
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w(time level hostname progname pid thread msg)
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("message")

          r.gets.should be_nil
        end
      end
    end

    context "with progname" do
      it "yields" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info("PROGNAME") { "mess" + "age" }
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w(time level hostname progname pid thread msg)
          actual["level"].should eq("info")
          actual["progname"].should eq("PROGNAME")
          actual["msg"].should eq("message")

          r.gets.should be_nil
        end
      end
    end

    context "with extra" do
      it "yields" do
        IO.pipe do |r, w|
          logger = Soma::Logger.new(w, progname: "progname")
          logger.info { {"mess" + "age", {foo: "foo", bar: 1}} }
          logger.close

          actual = JSON.parse(r.gets.to_s).as_h
          actual.keys.should eq %w(foo bar time level hostname progname pid thread msg)
          actual["level"].should eq("info")
          actual["progname"].should eq("progname")
          actual["msg"].should eq("message")
          actual["foo"].should eq("foo")
          actual["bar"].should eq(1)

          r.gets.should be_nil
        end
      end
    end
  end

  describe "logging" do
    context "in DEBUG level" do
      it "logs with higher levels than DEBUG" do
        IO.pipe do |r, w|
          logging(w, Logger::DEBUG)

          r.gets.should match(/"level":"debug".*"msg":"debug"/)
          r.gets.should match(/"level":"info".*"msg":"info"/)
          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"debug".*"msg":"debug"/)
          r.gets.should match(/"level":"info".*"msg":"info"/)
          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end

    context "in INFO level" do
      it "logs with higher levels than INFO" do
        IO.pipe do |r, w|
          logging(w, Logger::INFO)

          r.gets.should match(/"level":"info".*"msg":"info"/)
          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"info".*"msg":"info"/)
          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end

    context "in WARN level" do
      it "logs with higher levels than WARN" do
        IO.pipe do |r, w|
          logging(w, Logger::WARN)

          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"warn".*"msg":"warn"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end

    context "in ERROR level" do
      it "logs with higher levels than ERROR" do
        IO.pipe do |r, w|
          logging(w, Logger::ERROR)

          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"error".*"msg":"error"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end

    context "in FATAL level" do
      it "logs with higher levels than FATAL" do
        IO.pipe do |r, w|
          logging(w, Logger::FATAL)

          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"fatal".*"msg":"fatal"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end

    context "in UNKNOWN level" do
      it "logs with higher levels than UNKNOWN" do
        IO.pipe do |r, w|
          logging(w, Logger::UNKNOWN)

          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should match(/"level":"any".*"msg":"unknown"/)
          r.gets.should be_nil
        end
      end
    end
  end
end
