require "./spec_helper"

describe Soma do
  it "has a version number" do
    Soma::VERSION.should_not be_empty
  end

  describe ".logger" do
    it "returns a singleton instance of Soma::Logger class" do
      Soma.logger.should be_a Soma::Logger
      Soma.logger.should eq Soma.logger
    end
  end
end
