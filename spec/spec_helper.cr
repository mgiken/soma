require "spec"
require "../src/soma"

def logging(io, level)
  logger = Soma::Logger.new(io, level)

  logger.debug("debug")
  logger.info("info")
  logger.warn("warn")
  logger.error("error")
  logger.fatal("fatal")
  logger.unknown("unknown")

  logger.log(Logger::DEBUG, "debug")
  logger.log(Logger::INFO, "info")
  logger.log(Logger::WARN, "warn")
  logger.log(Logger::ERROR, "error")
  logger.log(Logger::FATAL, "fatal")
  logger.log(Logger::UNKNOWN, "unknown")

  logger.close
end
